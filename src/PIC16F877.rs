use std::vec::Vec;

use crate::IntelHEX::RecordType;
use crate::IntelHEX::ParsedHEX;

use crate::PIC16F877_opcodes::OpCode;
use crate::PIC16F877_opcodes::DecodedInstruction;
use crate::PIC16F877_instructions::InstructionSet;
use crate::PIC16F877_mem::STATUS_BitAliases;
use crate::PIC16F877_mem::REGISTER_8b;
use crate::PIC16F877_mem::REGISTER_14b;
use crate::PIC16F877_mem::Memory;
use crate::PIC16F877_mem::Set;

pub struct PIC16F877 {
    program: Vec::<DecodedInstruction>,
    bankID: REGISTER_8b,
    reg_STATUS: REGISTER_8b,
    reg_W: REGISTER_8b,
    mem: Memory,
}

impl PIC16F877 {
    pub fn parse(&mut self, hex: ParsedHEX) {
        for line in hex.source {
            match line.record_type {
                RecordType::DATA => {
                    let mut sub_prog = self.decode_line(line.data);
                    self.program.append(&mut sub_prog);
                },
                RecordType::EOF  => break,
                // Let's just ignore other types for now
                _ => (),
            }
        }
    }

    pub fn execute(&mut self) {
        #[allow(non_snake_case)]
        let mut programCounter = 0;
        loop {
            if programCounter >= self.program.len() {
                break;
            }

            let status: u8 = *(self.reg_STATUS);
            let rp0: u8 = STATUS_BitAliases::RP0.into();
            let rp1: u8 = STATUS_BitAliases::RP1.into();
            self.bankID.set(0x00, (status & rp0) != 0);
            self.bankID.set(0x01, (status & rp1) != 0);

            let p = &self.program[programCounter];
            p.execute(self.bankID, self.reg_STATUS, self.reg_W, &self.mem);

            programCounter += 1;
        }
        ()
    }

    fn init(&mut self) {
        self.reg_STATUS.set( (1 << 0) | (1 << 1) | (1 << 2) | (1 << 3) | (1 << 4) | (1 << 5) | (1 << 6) | (1 << 7), false);
        self.reg_W.set( (1 << 0) | (1 << 1) | (1 << 2) | (1 << 3) | (1 << 4) | (1 << 5) | (1 << 6) | (1 << 7), false);

        self.mem.Bank0.lowFree  = REGISTER_14b(0x0020);
        self.mem.Bank0.highFree = REGISTER_14b(0x007F);
        self.mem.Bank1.lowFree  = REGISTER_14b(0x00A0);
        self.mem.Bank1.highFree = REGISTER_14b(0x00EF);
        self.mem.Bank2.lowFree  = REGISTER_14b(0x0110);
        self.mem.Bank2.highFree = REGISTER_14b(0x016F);
        self.mem.Bank3.lowFree  = REGISTER_14b(0x0190);
        self.mem.Bank3.highFree = REGISTER_14b(0x01EF);
    }

    fn decode_line(&self, data: Vec::<u8>) -> Vec::<DecodedInstruction>{
        let mut rv = Vec::new();
        for pair in data.chunks(2) {
            let decoded = self.decode_instruction(pair);
            rv.push(decoded);
        }
        rv
    }

    fn decode_instruction(&self, pair: &[u8]) -> DecodedInstruction {
        let payload = ((pair[1] as u16) << 8) | pair[0] as u16;
        for inst in InstructionSet {
            let opcode = payload & inst.mask;
            let maybe_opcode: OpCode = opcode.into();
            if maybe_opcode == inst.opcode {
                return DecodedInstruction {
                    opcode: maybe_opcode,
                    op1: (payload & inst.operande1),
                    op2: (payload & inst.operande2) >> 7,
                };
            }
        }
        DecodedInstruction {
            opcode: OpCode::INVALID,
            op1: 0x00,
            op2: 0x00,
        }
    }
}

pub fn new(hex: ParsedHEX) -> PIC16F877 {
    let mut m = PIC16F877 {
        program: Vec::<DecodedInstruction>::new(),
        bankID: REGISTER_8b(0),
        reg_STATUS: REGISTER_8b(0),
        reg_W: REGISTER_8b(0),
        mem: Memory::new(),
    };
    m.parse(hex);
    m.init();
    m
}
