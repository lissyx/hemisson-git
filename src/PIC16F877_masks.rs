use std::ops::BitAnd;

#[derive(Debug, Copy, Clone)]
#[allow(non_camel_case_types)]
pub enum OpCodeMask {
    NOP         = 0x3F9F,
    BYTE_fd_BIN = 0x3F00,
    BYTE_f      = 0x3F80,
    BIT_fb_BIN  = 0x3C00,
    BIN_ADD_SUB = 0x3E00,
    JUMPS       = 0x3800,
    CLEAR       = 0x3FFF,
    ALL         = 0xFFFF,
    NONE        = 0x0000,
}

impl From<OpCodeMask> for u16 {
    fn from(mask: OpCodeMask) -> Self {
        match mask {
            OpCodeMask::NOP         => 0x3F9F,
            OpCodeMask::BYTE_fd_BIN => 0x3F00,
            OpCodeMask::BYTE_f      => 0x3F80,
            OpCodeMask::BIT_fb_BIN  => 0x3C00,
            OpCodeMask::BIN_ADD_SUB => 0x3E00,
            OpCodeMask::JUMPS       => 0x3800,
            OpCodeMask::CLEAR       => 0x3FFF,
            OpCodeMask::ALL         => 0xFFFF,
            OpCodeMask::NONE        => 0x0000,
        }
    }
}

impl BitAnd<OpCodeMask> for u16 {
    type Output = u16;
    #[inline]
    fn bitand(self, rhs: OpCodeMask) -> Self::Output {
        let rhs_u16: u16 = rhs.into();
        self & rhs_u16
    }
}

#[derive(Debug, Copy, Clone)]
#[allow(non_camel_case_types)]
pub enum OperandeMask {
    NONE          = 0x0000,
    BYTE_OR_BIT_f = 0x007F,
    BYTE_d        = 0x0080,
    BIT_b         = 0x0380,
    LITERAL_k     = 0x00FF,
    MEM_k         = 0x03FF,
    CONTROL_k     = 0x07FF,
}

impl From<OperandeMask> for u16 {
    fn from(mask: OperandeMask) -> Self {
        match mask {
            OperandeMask::NONE          => 0x0000,
            OperandeMask::BYTE_OR_BIT_f => 0x007F,
            OperandeMask::BYTE_d        => 0x0080,
            OperandeMask::BIT_b         => 0x0380,
            OperandeMask::LITERAL_k     => 0x00FF,
            OperandeMask::MEM_k         => 0x03FF,
            OperandeMask::CONTROL_k     => 0x07FF,
        }
    }
}

impl BitAnd<OperandeMask> for u16 {
    type Output = u16;
    #[inline]
    fn bitand(self, rhs: OperandeMask) -> u16 {
        let rhs_u16: u16 = rhs.into();
        self & rhs_u16
    }
}
