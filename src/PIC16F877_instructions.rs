use crate::PIC16F877_opcodes::OpCode;
use crate::PIC16F877_opcodes::CodedInstruction;
use crate::PIC16F877_masks::OpCodeMask;
use crate::PIC16F877_masks::OperandeMask;

#[allow(non_upper_case_globals)]
pub static InstructionSet: &[CodedInstruction] = &[
    CodedInstruction {
        opcode:    OpCode::INVALID,
        mask:      OpCodeMask::NONE,
        operande1: OperandeMask::NONE,
        operande2: OperandeMask::NONE,
    },
    CodedInstruction {
        opcode:    OpCode::ADDWF,
        mask:      OpCodeMask::BYTE_fd_BIN,
        operande1: OperandeMask::BYTE_OR_BIT_f,
        operande2: OperandeMask::BYTE_d,
    },
    CodedInstruction {
        opcode:    OpCode::ANDWF,
        mask:      OpCodeMask::BYTE_fd_BIN,
        operande1: OperandeMask::BYTE_OR_BIT_f,
        operande2: OperandeMask::BYTE_d,
    },
    CodedInstruction {
        opcode:    OpCode::CLRF,
        mask:      OpCodeMask::BYTE_f,
        operande1: OperandeMask::BYTE_OR_BIT_f,
        operande2: OperandeMask::NONE,
    },
    CodedInstruction {
        opcode:    OpCode::CLRW,
        mask:      OpCodeMask::BYTE_f,
        operande1: OperandeMask::NONE,
        operande2: OperandeMask::NONE,
    },
    CodedInstruction {
        opcode:    OpCode::COMF,
        mask:      OpCodeMask::BYTE_fd_BIN,
        operande1: OperandeMask::BYTE_OR_BIT_f,
        operande2: OperandeMask::BYTE_d,
    },
    CodedInstruction {
        opcode:    OpCode::DECF,
        mask:      OpCodeMask::BYTE_fd_BIN,
        operande1: OperandeMask::BYTE_OR_BIT_f,
        operande2: OperandeMask::BYTE_d,
    },
    CodedInstruction {
        opcode:    OpCode::DECFSZ,
        mask:      OpCodeMask::BYTE_fd_BIN,
        operande1: OperandeMask::BYTE_OR_BIT_f,
        operande2: OperandeMask::BYTE_d,
    },
    CodedInstruction {
        opcode:    OpCode::INCF,
        mask:      OpCodeMask::BYTE_fd_BIN,
        operande1: OperandeMask::BYTE_OR_BIT_f,
        operande2: OperandeMask::BYTE_d,
    },
    CodedInstruction {
        opcode:    OpCode::INCFSZ,
        mask:      OpCodeMask::BYTE_fd_BIN,
        operande1: OperandeMask::BYTE_OR_BIT_f,
        operande2: OperandeMask::BYTE_d,
    },
    CodedInstruction {
        opcode:    OpCode::IORWF,
        mask:      OpCodeMask::BYTE_fd_BIN,
        operande1: OperandeMask::BYTE_OR_BIT_f,
        operande2: OperandeMask::BYTE_d,
    },
    CodedInstruction {
        opcode:    OpCode::MOVF,
        mask:      OpCodeMask::BYTE_fd_BIN,
        operande1: OperandeMask::BYTE_OR_BIT_f,
        operande2: OperandeMask::BYTE_d,
    },
    CodedInstruction {
        opcode:    OpCode::MOVWF,
        mask:      OpCodeMask::BYTE_f,
        operande1: OperandeMask::BYTE_OR_BIT_f,
        operande2: OperandeMask::NONE,
    },
    CodedInstruction {
        opcode:    OpCode::NOP,
        mask:      OpCodeMask::NOP,
        operande1: OperandeMask::NONE,
        operande2: OperandeMask::NONE,
    },
    CodedInstruction {
        opcode:    OpCode::RLF,
        mask:      OpCodeMask::BYTE_fd_BIN,
        operande1: OperandeMask::BYTE_OR_BIT_f,
        operande2: OperandeMask::BYTE_d,
    },
    CodedInstruction {
        opcode:    OpCode::RRF,
        mask:      OpCodeMask::BYTE_fd_BIN,
        operande1: OperandeMask::BYTE_OR_BIT_f,
        operande2: OperandeMask::BYTE_d,
    },
    CodedInstruction {
        opcode:    OpCode::SUBWF,
        mask:      OpCodeMask::BYTE_fd_BIN,
        operande1: OperandeMask::BYTE_OR_BIT_f,
        operande2: OperandeMask::BYTE_d,
    },
    CodedInstruction {
        opcode:    OpCode::SWAPF,
        mask:      OpCodeMask::BYTE_fd_BIN,
        operande1: OperandeMask::BYTE_OR_BIT_f,
        operande2: OperandeMask::BYTE_d,
    },
    CodedInstruction {
        opcode:    OpCode::XORWF,
        mask:      OpCodeMask::BYTE_fd_BIN,
        operande1: OperandeMask::BYTE_OR_BIT_f,
        operande2: OperandeMask::BYTE_d,
    },
    CodedInstruction {
        opcode:    OpCode::BCF,
        mask:      OpCodeMask::BIT_fb_BIN,
        operande1: OperandeMask::BYTE_OR_BIT_f,
        operande2: OperandeMask::BIT_b,
    },
    CodedInstruction {
        opcode:    OpCode::BSF,
        mask:      OpCodeMask::BIT_fb_BIN,
        operande1: OperandeMask::BYTE_OR_BIT_f,
        operande2: OperandeMask::BIT_b,
    },
    CodedInstruction {
        opcode:    OpCode::BTFSC,
        mask:      OpCodeMask::BIT_fb_BIN,
        operande1: OperandeMask::BYTE_OR_BIT_f,
        operande2: OperandeMask::BIT_b,
    },
    CodedInstruction {
        opcode:    OpCode::BTFSS,
        mask:      OpCodeMask::BIT_fb_BIN,
        operande1: OperandeMask::BYTE_OR_BIT_f,
        operande2: OperandeMask::BIT_b,
    },
    CodedInstruction {
        opcode:    OpCode::ADDLW,
        mask:      OpCodeMask::BIN_ADD_SUB,
        operande1: OperandeMask::LITERAL_k,
        operande2: OperandeMask::NONE,
    },
    CodedInstruction {
        opcode:    OpCode::ANDLW,
        mask:      OpCodeMask::BYTE_fd_BIN,
        operande1: OperandeMask::LITERAL_k,
        operande2: OperandeMask::NONE,
    },
    CodedInstruction {
        opcode:    OpCode::CALL,
        mask:      OpCodeMask::JUMPS,
        operande1: OperandeMask::CONTROL_k,
        operande2: OperandeMask::NONE,
    },
    CodedInstruction {
        opcode:    OpCode::CLRWDT,
        mask:      OpCodeMask::CLEAR,
        operande1: OperandeMask::NONE,
        operande2: OperandeMask::NONE,
    },
    CodedInstruction {
        opcode:    OpCode::GOTO,
        mask:      OpCodeMask::JUMPS,
        operande1: OperandeMask::CONTROL_k,
        operande2: OperandeMask::NONE,
    },
    CodedInstruction {
        opcode:    OpCode::IORLW,
        mask:      OpCodeMask::BYTE_fd_BIN,
        operande1: OperandeMask::LITERAL_k,
        operande2: OperandeMask::NONE,
    },
    CodedInstruction {
        opcode:    OpCode::MOVLW,
        mask:      OpCodeMask::BIT_fb_BIN,
        operande1: OperandeMask::MEM_k,
        operande2: OperandeMask::NONE,
    },
    CodedInstruction {
        opcode:    OpCode::RETFIE,
        mask:      OpCodeMask::CLEAR,
        operande1: OperandeMask::NONE,
        operande2: OperandeMask::NONE,
    },
    CodedInstruction {
        opcode:    OpCode::RETLW,
        mask:      OpCodeMask::BIT_fb_BIN,
        operande1: OperandeMask::MEM_k,
        operande2: OperandeMask::NONE,
    },
    CodedInstruction {
        opcode:    OpCode::RETURN,
        mask:      OpCodeMask::CLEAR,
        operande1: OperandeMask::NONE,
        operande2: OperandeMask::NONE,
    },
    CodedInstruction {
        opcode:    OpCode::SLEEP,
        mask:      OpCodeMask::CLEAR,
        operande1: OperandeMask::NONE,
        operande2: OperandeMask::NONE,
    },
    CodedInstruction {
        opcode:    OpCode::SUBLW,
        mask:      OpCodeMask::BIN_ADD_SUB,
        operande1: OperandeMask::LITERAL_k,
        operande2: OperandeMask::NONE,
    },
    CodedInstruction {
        opcode:    OpCode::XORLW,
        mask:      OpCodeMask::BYTE_fd_BIN,
        operande1: OperandeMask::LITERAL_k,
        operande2: OperandeMask::NONE,
    },
];
