use std::vec::Vec;

use std::io;
use std::io::BufRead;

use std::fs::File;
use std::path::Path;

#[derive(Debug)]
#[allow(non_camel_case_types)]
pub enum RecordType {
    DATA,
    EOF,
    EXTENDED_SEGMENT,
    START_SEGMENT,
    EXTENDED_LINEAR,
    START_LINEAR,
    INVALID,
}

#[derive(Debug)]
pub struct Line {
    pub record_type: RecordType,
    pub address: u16,
    pub data: Vec::<u8>,
}

pub struct ParsedHEX<'life_asm> {
    hex_file: &'life_asm str,
    pub source: Vec::<Line>,
}

impl ParsedHEX<'_> {
    pub fn new(src_hex_file: &str) -> ParsedHEX {
        ParsedHEX {
            hex_file: src_hex_file,
            source: Vec::<Line>::new(),
        }
    }

    pub fn load(&mut self) {
        if let Ok(lines) = read_lines(self.hex_file) {
            for line in lines {
                if let Ok(infos) = line {
                    if let Ok(pline) = self.parse_line(infos) {
                        self.source.push(pline);
                    }
                }
            }
        }
    }

    /**
     * : BB AAAA TT [DDDDDDDD] CC
     *
     * where
     * : is start of line marker
     * BB is number of data bytes on line
     * AAAA is address in bytes
     * TT is type.
     *   - 00 means data,
     *   - 01 means EOF,
     *   - 02 means linear address
     *   - 04 means extended address
     * DD is data bytes, number depends on BB value
     * CC is checksum (2s-complement of number of bytes+address+data)
     * 
     * */

    fn parse_line(&mut self, line: String) -> Result<Line, &str> {
        match line.get(..1) {
            Some(":") => {
                let (bytes, address, rtype, data, _crc) = self.pull_from_line(line);

                if !self.validate_crc(bytes, address, rtype, data.clone()) {
                    Err("Invalid CRC")
                } else {
                    let rline = Line {
                        record_type: match rtype {
                            0 => RecordType::DATA,
                            1 => RecordType::EOF,
                            2 => RecordType::EXTENDED_SEGMENT,
                            3 => RecordType::START_SEGMENT,
                            4 => RecordType::EXTENDED_LINEAR,
                            5 => RecordType::START_LINEAR,
                            _ => RecordType::INVALID,
                        },
                        address: address,
                        data: data,
                    };
                    Ok(rline)
                }
            },
            None      => Err("Should start with ':' but got None"),
            _         => Err("Should start with ':' but got Some(...)"),
        }
    }

    fn pull_from_line(&mut self, line: String) -> (u8, u16, u8, Vec::<u8>, u8) {
        let s_bytes       = line.get(1..3);
        let _bytes: u8    = u8::from_str_radix(s_bytes.unwrap(), 16).unwrap();

        let s_address     = line.get(3..7);
        let _address: u16 = u16::from_str_radix(s_address.unwrap(), 16).unwrap();

        let s_type        = line.get(7..9);
        let _type: u8     = u8::from_str_radix(s_type.unwrap(), 16).unwrap();

        let s_data        = line.get(9..9 + 2*usize::from(_bytes));
        let _data         = from_string(s_data.unwrap(), 2);

        let s_crc         = line.get(9 + 2*usize::from(_bytes)..);
        let _crc          = u8::from_str_radix(s_crc.unwrap(), 16).unwrap();

        (_bytes, _address, _type, _data, _crc)
    }

    fn validate_crc(&mut self, bytes: u8, address: u16, rtype: u8, mut data: Vec::<u8>) -> bool {
        // println!("crc={:0>8b} {:} (read)", crc, crc);
        let address_be = address.to_be_bytes();
        let mut crc_vec = Vec::new();
        crc_vec.push(bytes);
        crc_vec.push(address_be[0]);
        crc_vec.push(address_be[1]);
        crc_vec.push(rtype);
        crc_vec.append(&mut data);
        // println!("crc_vec={:?})", crc_vec);
        let crc_sum: u32 = crc_vec.iter().map(|x| *x as u32).sum();
        // println!("crc={:0>8b} {:} (lsb)", crc_sum, crc_sum);
        let final_crc: u8 = !(crc_sum.to_be_bytes()[0]);
        // println!("crc={:0>8b} {:} (!lsb)", final_crc, final_crc);
        let total: u8 = (crc_sum + u32::from(final_crc)).to_be_bytes()[0];
        // println!("crc={:0>8b} {:} (total)", total, total);
        if total == 0 {
            true
        } else {
            false
        }
    }
}


// The output is wrapped in a Result to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

fn from_string(string: &str, sub_len: usize) -> Vec<u8> {
    let mut subs = Vec::with_capacity(string.len() / sub_len);
    let mut iter = string.chars();
    let mut pos = 0;

    while pos < string.len() {
        let mut len = 0;
        for ch in iter.by_ref().take(sub_len) {
            len += ch.len_utf8();
        }
        subs.push(u8::from_str_radix(&string[pos..pos + len], 16).unwrap());
        pos += len;
    }
    subs
}
