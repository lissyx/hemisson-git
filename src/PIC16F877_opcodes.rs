use crate::PIC16F877_masks::OpCodeMask;
use crate::PIC16F877_masks::OperandeMask;

use crate::PIC16F877_mem::STATUS_BitAliases;
use crate::PIC16F877_mem::REGISTER_8b;
use crate::PIC16F877_mem::REGISTER_14b;
use crate::PIC16F877_mem::Memory;
use crate::PIC16F877_mem::Set;

#[derive(Debug, PartialEq)]
pub enum OpCode {
    /* Fake OpCode to handle the INVALID case */
    INVALID = 0xFFFF,    /* 1111 1111 1111 1111 */

    ADDWF   = 0x0700,    /* 0000 0111 0000 0000 */
    ANDWF   = 0x0500,    /* 0000 0101 0000 0000 */    
    CLRF    = 0x0180,    /* 0000 0001 1000 0000 */
    CLRW    = 0x0100,    /* 0000 0001 0000 0000 */
    COMF    = 0x0900,    /* 0000 1001 0000 0000 */
    DECF    = 0x0300,    /* 0000 0011 0000 0000 */
    DECFSZ  = 0x0B00,    /* 0000 1011 0000 0000 */
    INCF    = 0x0A00,    /* 0000 1010 0000 0000 */
    INCFSZ  = 0x0F00,    /* 0000 1111 0000 0000 */
    IORWF   = 0x0400,    /* 0000 0100 0000 0000 */
    MOVF    = 0x0800,    /* 0000 1000 0000 0000 */
    MOVWF   = 0x0080,    /* 0000 0000 1000 0000 */
    NOP     = 0x0000,    /* 0000 0000 0000 0000 */
    RLF     = 0x0D00,    /* 0000 1101 0000 0000 */
    RRF     = 0x0C00,    /* 0000 1100 0000 0000 */
    SUBWF   = 0x0200,    /* 0000 0010 0000 0000 */
    SWAPF   = 0x0E00,    /* 0000 1110 0000 0000 */
    XORWF   = 0x0600,    /* 0000 0110 0000 0000 */

    BCF     = 0x1000,    /* 0001 0000 0000 0000 */
    BSF     = 0x1400,    /* 0001 0100 0000 0000 */
    BTFSC   = 0x1800,    /* 0001 1000 0000 0000 */
    BTFSS   = 0x1C00,    /* 0001 1100 0000 0000 */

    ADDLW   = 0x3E00,    /* 0011 1110 0000 0000 */
    ANDLW   = 0x3900,    /* 0011 1001 0000 0000 */
    CALL    = 0x2000,    /* 0010 0000 0000 0000 */
    CLRWDT  = 0x0064,    /* 0000 0000 0110 0100 */
    GOTO    = 0x2800,    /* 0010 1000 0000 0000 */
    IORLW   = 0x3800,    /* 0011 1000 0000 0000 */
    MOVLW   = 0x3000,    /* 0011 0000 0000 0000 */
    RETFIE  = 0x0009,    /* 0000 0000 0000 1001 */
    RETLW   = 0x3400,    /* 0011 0100 0000 0000 */
    RETURN  = 0x0008,    /* 0000 0000 0000 1000 */
    SLEEP   = 0x0063,    /* 0000 0000 0110 0011 */
    SUBLW   = 0x3C00,    /* 0011 1100 0000 0000 */
    XORLW   = 0x3A00,    /* 0011 1010 0000 0000 */
}

impl From<u16> for OpCode {
    fn from(opcode: u16) -> Self {
        match opcode {
            0x0700 => OpCode::ADDWF,
            0x0500 => OpCode::ANDWF,
            0x0180 => OpCode::CLRF,
            0x0100 => OpCode::CLRW,
            0x0900 => OpCode::COMF,
            0x0300 => OpCode::DECF,
            0x0B00 => OpCode::DECFSZ,
            0x0A00 => OpCode::INCF,
            0x0F00 => OpCode::INCFSZ,
            0x0400 => OpCode::IORWF,
            0x0800 => OpCode::MOVF,
            0x0080 => OpCode::MOVWF,
            0x0000 => OpCode::NOP,
            0x0D00 => OpCode::RLF,
            0x0C00 => OpCode::RRF,
            0x0200 => OpCode::SUBWF,
            0x0E00 => OpCode::SWAPF,
            0x0600 => OpCode::XORWF,

            0x1000 => OpCode::BCF,
            0x1400 => OpCode::BSF,
            0x1800 => OpCode::BTFSC,
            0x1C00 => OpCode::BTFSS,

            0x3E00 => OpCode::ADDLW,
            0x3900 => OpCode::ANDLW,
            0x2000 => OpCode::CALL,
            0x0064 => OpCode::CLRWDT,
            0x2800 => OpCode::GOTO,
            0x3800 => OpCode::IORLW,
            0x3000 => OpCode::MOVLW,
            0x0009 => OpCode::RETFIE,
            0x3400 => OpCode::RETLW,
            0x0008 => OpCode::RETURN,
            0x0063 => OpCode::SLEEP,
            0x3C00 => OpCode::SUBLW,
            0x3A00 => OpCode::XORLW,

            _      => OpCode::INVALID,
        }
    }
}

//// Microchip doc, DS30292C, p136-137

#[derive(Debug)]
pub struct CodedInstruction {
    pub opcode:    OpCode,
    pub mask:      OpCodeMask,
    pub operande1: OperandeMask,
    pub operande2: OperandeMask,
}

#[derive(Debug)]
pub struct DecodedInstruction {
    pub opcode: OpCode,
    pub op1:    u16,
    pub op2:    u16,
}

impl DecodedInstruction {
    pub fn movwf(&self, bankID: REGISTER_8b, mut status: REGISTER_8b, mut w: REGISTER_8b, mut mem: &Memory) {
    }

    pub fn nop(&self, bankID: REGISTER_8b, mut status: REGISTER_8b, mut w: REGISTER_8b, mut mem: &Memory) {
    }

    pub fn bcf(&self, bankID: REGISTER_8b, status: REGISTER_8b, w: REGISTER_8b, mem: &Memory) {
        /* Met à 0 le bit op2 de op1 */
        /* op2 indique le bit, 0 étant le bit 0 */
        let mut tmp = mem.read(bankID, REGISTER_14b(self.op1));
        tmp.set(1 << self.op2, false);
        mem.write(bankID, REGISTER_14b(self.op1), tmp)
    }

    pub fn goto(&self, bankID: REGISTER_8b, mut status: REGISTER_8b, mut w: REGISTER_8b, mut mem: &Memory) {
    }

    pub fn movlw(&self, bankID: REGISTER_8b, mut status: REGISTER_8b, mut w: REGISTER_8b, mut mem: &Memory) {
    }

    pub fn r#return(&self, bankID: REGISTER_8b, mut status: REGISTER_8b, mut w: REGISTER_8b, mut mem: &Memory) {
    }

    pub fn execute(&self, bankID: REGISTER_8b, mut status: REGISTER_8b, mut w: REGISTER_8b, mut mem: &Memory) {
        println!("{:?}\t{},{}", self.opcode, self.op1, self.op2);
        match self.opcode {
            OpCode::MOVWF   => self.movwf(bankID, status, w, mem),
            OpCode::NOP     => self.nop(bankID, status, w, mem),
            OpCode::BCF     => self.bcf(bankID, status, w, mem),
            OpCode::GOTO    => self.goto(bankID, status, w, mem),
            OpCode::MOVLW   => self.movlw(bankID, status, w, mem),
            OpCode::RETURN  => self.r#return(bankID, status, w, mem),
            OpCode::INVALID => (),
            _ => {
                println!("Unhandled execute: {:?}", self.opcode);
            }
        }
    }
}
