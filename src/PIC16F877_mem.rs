use std::fmt;
use std::ops::Deref;

#[derive(Copy, Clone)]
#[allow(non_camel_case_types)]
pub struct REGISTER_14b(pub u16);

#[derive(Copy, Clone)]
#[allow(non_camel_case_types)]
pub struct REGISTER_8b(pub u8);

impl Deref for REGISTER_14b {
    type Target = u16;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl Deref for REGISTER_8b {
    type Target = u8;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

pub trait Set {
    type BitsLen;
    fn set(&mut self, bits: Self::BitsLen, val: bool);
}

impl Set for REGISTER_8b {
    type BitsLen = u8;
    fn set(&mut self, bits: Self::BitsLen, val: bool) {
        let mut self_b = *(*self);
        if val == false {
            self_b &= !(bits | 0x00);
        } else {
            self_b |= bits | 0x01;
        }
        self.0 = self_b;
        ()
    }
}

impl Set for REGISTER_14b {
    type BitsLen = u16;
    fn set(&mut self, bits: Self::BitsLen, val: bool) {
        let mut self_b = *(*self);
        if val == false {
            self_b &= !(bits | 0x00);
        } else {
            self_b |= bits | 0x01;
        }
        self.0 = self_b;
        ()
    }
}

fn set_status_z(mut status: REGISTER_8b, reg: REGISTER_8b) {
    let reg_b = *reg;
	let val: bool = if (reg_b & (1 << 0) & (1 << 1) & (1 << 2) & (1 << 3) & (1 << 4) & (1 << 5) & (1 << 6) & (1 << 7)) == 0x00 {
		true
    } else {
		false
    };
	status.set(STATUS_BitAliases::Z.into(), val);
}

impl fmt::Display for REGISTER_8b {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let self_b = *(*self);
        write!(f,
               "REG-8b: [ {}, {}, {}, {}, {}, {}, {}, {} ]",
               (self_b >> 7) & 1,
               (self_b >> 6) & 1,
               (self_b >> 5) & 1,
               (self_b >> 4) & 1,
               (self_b >> 3) & 1,
               (self_b >> 2) & 1,
               (self_b >> 1) & 1,
               (self_b >> 0) & 1
        )
    }
}

impl fmt::Display for REGISTER_14b {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let self_b = *(*self);
        write!(f,
               "REG-14b: [ {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {} ]",
               (self_b >> 13) & 1,
               (self_b >> 12) & 1,
               (self_b >> 11) & 1,
               (self_b >> 10) & 1,
               (self_b >> 9) & 1,
               (self_b >> 8) & 1,
               (self_b >> 7) & 1,
               (self_b >> 6) & 1,
               (self_b >> 5) & 1,
               (self_b >> 4) & 1,
               (self_b >> 3) & 1,
               (self_b >> 2) & 1,
               (self_b >> 1) & 1,
               (self_b >> 0) & 1
        )
    }
}

#[allow(non_snake_case)]
#[derive(Copy, Clone)]
pub struct MemBank {
    reg: [REGISTER_8b; 127], 
    pub lowFree:  REGISTER_14b,
    pub highFree: REGISTER_14b,
}

#[allow(non_snake_case)]
#[derive(Copy, Clone)]
pub struct Memory {
    pub Bank0: MemBank,
    pub Bank1: MemBank,
    pub Bank2: MemBank,
    pub Bank3: MemBank,
}

impl Memory {
    pub fn new() -> Memory {
        Memory {
            Bank0: MemBank {
                reg: [REGISTER_8b(0); 127],
                lowFree: REGISTER_14b(0),
                highFree: REGISTER_14b(0),
            },
            Bank1: MemBank {
                reg: [REGISTER_8b(0); 127],
                lowFree: REGISTER_14b(0),
                highFree: REGISTER_14b(0),
            },
            Bank2: MemBank {
                reg: [REGISTER_8b(0); 127],
                lowFree: REGISTER_14b(0),
                highFree: REGISTER_14b(0),
            },
            Bank3: MemBank {
                reg: [REGISTER_8b(0); 127],
                lowFree: REGISTER_14b(0),
                highFree: REGISTER_14b(0),
            },
        }
    }

    #[allow(non_snake_case)]
    pub fn read(self, bankID: REGISTER_8b, pos: REGISTER_14b) -> REGISTER_8b {
        let bank = self.get_bank(bankID);

        if *pos < *(bank.lowFree) {
            println!("READ REGISTER: {}@{:#0x}", bankID, *pos);
            self.read_file(bank, pos)
        } else if *pos >= *(bank.lowFree) && *pos <= *(bank.highFree) {
            // Read/Write PIC zone
            bank.reg[*pos as usize]
        } else {
            // IO mapped
            println!("READ IO: {}@{:#0x}", bankID, *pos);
            REGISTER_8b(0x00)
        }
    }

    fn read_file(self, bank: MemBank, pos: REGISTER_14b) -> REGISTER_8b {
        if *pos == 0x0A || *pos == 0x8A || *pos == 0x10A || *pos == 0x18A {
            return REGISTER_8b(0x00);
        }
        REGISTER_8b(0x00)
    }

    #[allow(non_snake_case)]
    pub fn write(self, bankID: REGISTER_8b, pos: REGISTER_14b, val: REGISTER_8b) {
        let mut bank = self.get_bank(bankID);

        if *pos < *(bank.lowFree) {
            println!("WRITE REGISTER: {}@{:#0x}", bankID, *pos);
        } else if *pos >= *(bank.lowFree) && *pos <= *(bank.highFree) {
            bank.reg[*pos as usize] = val;
        } else {
            // IO mapped
            println!("WRITE IO: {}@{:#0x}", bankID, *pos);
        }
    }

    #[allow(non_snake_case)]
    fn get_bank(self, bankID: REGISTER_8b) -> MemBank {
        match *bankID {
            0x00 => self.Bank0,
            0x01 => self.Bank1,
            0x02 => self.Bank2,
            0x03 => self.Bank3,
            _ => panic!("Unsupported BankID"),
        }
    }
}

#[allow(non_camel_case_types)]
pub enum STATUS_BitAliases {
    IRP = 0x80,
    RP1 = 0x40,
    RP0 = 0x20,
    TO  = 0x10,
    PD  = 0x08,
    Z   = 0x04,
    DC  = 0x02,
    C   = 0x01,
}

#[allow(non_camel_case_types)]
impl From<STATUS_BitAliases> for u8 {
    fn from(mask: STATUS_BitAliases) -> Self {
        match mask {
            STATUS_BitAliases::IRP => 0x80,
            STATUS_BitAliases::RP1 => 0x40,
            STATUS_BitAliases::RP0 => 0x20,
            STATUS_BitAliases::TO  => 0x10,
            STATUS_BitAliases::PD  => 0x08,
            STATUS_BitAliases::Z   => 0x04,
            STATUS_BitAliases::DC  => 0x02,
            STATUS_BitAliases::C   => 0x01,
        }
    }
}

#[allow(non_camel_case_types)]
impl From<STATUS_BitAliases> for u16 {
    fn from(mask: STATUS_BitAliases) -> Self {
        match mask {
            STATUS_BitAliases::IRP => 0x80,
            STATUS_BitAliases::RP1 => 0x40,
            STATUS_BitAliases::RP0 => 0x20,
            STATUS_BitAliases::TO  => 0x10,
            STATUS_BitAliases::PD  => 0x08,
            STATUS_BitAliases::Z   => 0x04,
            STATUS_BitAliases::DC  => 0x02,
            STATUS_BitAliases::C   => 0x01,
        }
    }
}

#[allow(non_camel_case_types)]
enum RegisterFileMap
{
    /* *
     * Bank0 
     *
     * Les registres présents dans plusieurs banks sont
     * préfixés de bN_, avec N le numéro de banque.
     * */

    b0_TMR0  = 0x01,
    b0_PCL,
    b0_STATUS,
    b0_FSR,
    PORTA,
    b0_PORTB,
    PORTC,
    PORTD,
    PORTE,
    b0_PCLATH,
    b0_INTCON,
    PIR1,
    PIR2,
    TMR1L,
    TMR1H,
    T1CON    = 0x10,
    TMR2,
    T2CON,
    SSPBUF,
    SSDCON,
    CCPR1L,
    CCPR1H,
    CCP1CON,
    RCSTA,
    TXREG,
    RCREG,
    CCPR2L,
    CCPR2H,
    CCP2CON,
    ADRESH,
    ADCON0,

    /* Bank1 */
    b1_OPTION_REG = 0x81,
    b1_PCL,
    b1_STATUS,
    b1_FSR,
    TRISA,
    b1_TRISB,
    TRISC,
    TRISD,
    TRISE,
    b1_PCLATH,
    b1_INTCON,
    PIE1,
    PIE2,
    PCON,
    SSPCON2       = 0x91,
    PR2,
    SSPADD,
    SSPSTAT,
    TXSTA         = 0x98,
    SPBRG,
    ADRESL        = 0x9E,
    ADCON1        = 0x9F,

    /* Bank2 */
    b2_TMR0   = 0x101,
    b2_PCL,
    b2_STATUS,
    b2_FSR,
    b2_PORTB  = 0x106,
    b2_PCLATH = 0x10A,
    b2_INTCON,
    EEDATA,
    EEADR,
    EEDATH,
    EEADRH,

    /* Bank3 */
    b3_OPTION_REG = 0x181,
    b3_PCL,
    b3_STATUS,
    b3_FSR,
    b3_TRISB      = 0x186,
    b3_PCLATH     = 0x18A,
    b3_INTCON,
    EECON1,
    EECON2
}

#[allow(non_camel_case_types)]
enum FunctionPointerRegister
{
    FPTR_LOW    = 0x95,
    FPTR_HIGH,
    FPTR_COMMIT
}
