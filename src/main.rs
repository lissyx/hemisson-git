use clap::{Arg, App};

#[allow(non_snake_case)]
mod IntelHEX;
#[allow(non_snake_case)]
mod PIC16F877;
#[allow(non_snake_case)]
mod PIC16F877_opcodes;
#[allow(non_snake_case)]
mod PIC16F877_masks;
#[allow(non_snake_case)]
mod PIC16F877_instructions;
#[allow(non_snake_case)]
mod PIC16F877_mem;

static EMU_VER: &str = "0.8";

fn main() {
    let cli_args = App::new("PIC16F877 emulator")
        .version(EMU_VER)
        .about("A PoC PIC16F877 emulator allowing for multi-process with physical device communication")
        .arg(Arg::new("device")
             .short('d')
             .long("device")
             .value_name("FILE")
             .about("Path to device character allowing communication with Hemisson")
             .takes_value(true))
        .arg(Arg::new("HEX")
             .about("HEX compiled file to run")
             .required(true))
        .get_matches();
    
    let hex_bin_file: &str = if let Some(hex_bin) = cli_args.value_of("HEX") {
        println!("Reading from {}", hex_bin);
        hex_bin
    } else {
        return;
    };

    // load_pic16f877_asm(hex_bin)
    let mut prog = IntelHEX::ParsedHEX::new(hex_bin_file);
    prog.load();
    let mut hemisson = PIC16F877::new(prog);
    hemisson.execute();
}
